__author__ = '懷民'

from tkinter import *
import random

root = Tk()
root.wm_title("RSA")

def getBigNum(bit):
    st = "1"
    for i in range(bit-2):
        st += str(random.randint(0, 1))
    st += "1"
    return st

def testPrime(st):
    num = int(st, 2) - 1
    k = 0
    while True:
        if num % pow(2, k) != 0:
            break
        k += 1
    k -= 1
    m = num // pow(2, k)
    count = 0
    while count < 4:
        a = random.randint(2, num - 1)
        b = Sq_and_Mu(a, m, num + 1)
        if b != 1 and b != num:
            i = 1
            while i < k and b != num:
                b = pow(b, 2) % (num + 1)
                if b == 1:
                    return False
                i += 1
            if b != num:
                return False
        count += 1
    return True

def Sq_and_Mu(x, H, n): #output x^H mod n
    h_bin = '{0:04b}'.format(H)
    y = x
    for i in range(1, len(h_bin)):
        y = pow(y, 2) % n
        if h_bin[i] == "1":
            y = (y * x) % n
    return y

def Ext_Euclid (whi_n, e):
    if e == 0:
        return 1, 0, whi_n
    else:
        x, y, q = Ext_Euclid(e, whi_n % e)
        x, y = y, (x - (whi_n // e) * y)
        return x, y, q

def encode():
    if entryLength.get() != "" and txtPlain.get("1.0", END) != "":
        x = int(txtPlain.get("1.0", END))
        n_len = int(entryLength.get())
        while True:
            p = getBigNum(n_len // 2)
            if testPrime(p):
                break
        while True:
            q = getBigNum(n_len // 2)
            if testPrime(q):
                break
        p, q = int(p, 2), int(q, 2)
        n = p * q
        whi_n = (p-1) * (q-1)
        e = 65537
        d = Ext_Euclid(whi_n, e)[1] % whi_n

        y = Sq_and_Mu(x, e, n)

        txtKpub.delete(1.0, END)
        txtKpub.insert(END, "(" + str(n) + ", " + str(e) + ")")
        txtKpri.delete(1.0, END)
        txtKpri.insert(END, d)
        txtCipher.delete(1.0, END)
        txtCipher.insert(END, y)

def decode():
    if entryLength.get() != "" and txtCipher.get("1.0", END) != "" and txtKpri.get("1.0", END) != "" and txtKpub.get("1.0", END) != "":
        y = txtCipher.get("1.0", END)
        d = txtKpri.get("1.0", END)
        pub = txtKpub.get("1.0", END)
        for i in range(len(pub)):
            if pub[i] == ",":
                pos = i
                break
        n = int(pub[1:pos])
        x = Sq_and_Mu(int(y), int(d), n)

        txtPlain.delete(1.0, END)
        txtPlain.insert(END, x)


lblLength = Label(root, text="Length of N：").grid(row=0, sticky=E)
lblPlain = Label(root, text="Plaintext：").grid(row=1, sticky=E)
lblCipher = Label(root, text="Ciphertext：").grid(row=2, sticky=E)
lblKpub = Label(root, text="Public Key：").grid(row=3, sticky=E)
lblKpri = Label(root, text="Private Key：").grid(row=4, sticky=E)
entryLength = Entry(root)
entryLength.focus_set()
txtPlain = Text(root, height=10)
txtCipher = Text(root, height=10)
txtKpub = Text(root, height=10)
txtKpri = Text(root, height=10)
entryLength.grid(row=0, column=1, sticky=W)
txtPlain.grid(row=1, column=1, ipadx=200)
txtCipher.grid(row=2, column=1, ipadx=200)
txtKpub.grid(row=3, column=1, ipadx=200)
txtKpri.grid(row=4, column=1, ipadx=200)
btnEncode = Button(root, text="Encode", command=encode).grid(row=5, column=0)
btnDecode = Button(root, text="Decode", command=decode).grid(row=5, column=1)

root.mainloop()